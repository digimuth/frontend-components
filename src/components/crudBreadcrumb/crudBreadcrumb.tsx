import {Breadcrumb} from "antd";
import React, {createContext, useContext} from "react";
import {Link, useLocation} from "react-router-dom";

const BreadcrumbContext = createContext<string | null>(null);

export default function CrudBreadcrumb({routingDefinitions}: CrudBreadcrumbProps) {
    const location = useLocation();
    const path = location.pathname;
    const items = generateBreadcrumbEntries(path, routingDefinitions);
    const context = useContext(BreadcrumbContext);

    function matchRoute(path: string, route: string): { [key: string]: string } | null {
        const pathSegments = path.split('/').filter(Boolean);
        const routeSegments = route.split('/').filter(Boolean);

        if (pathSegments.length !== routeSegments.length) {
            return null;
        }

        const params: { [key: string]: string } = {};

        for (let i = 0; i < routeSegments.length; i++) {
            if (routeSegments[i].startsWith(':')) {
                const paramName = routeSegments[i].substring(1);
                params[paramName] = pathSegments[i];
            } else if (routeSegments[i] !== pathSegments[i]) {
                return null;
            }
        }

        return params;
    }

    function generateBreadcrumbEntries(path: string, routingDefinitions: { [key: string]: RoutingDefinition }) {
        for (const key in routingDefinitions) {
            const { route, breadcrumbEntries } = routingDefinitions[key];
            const params = matchRoute(path, route);

            if (params && breadcrumbEntries) {
                return breadcrumbEntries.map(entry => ({
                    name: entry.name,
                    route: entry.route ? Object.keys(params).reduce((acc, param) => acc.replace(`:${param}`, params[param]), entry.route) : undefined
                }));
            }
        }

        return null;
    }

    if (!items?.length) {
        return null;
    }

    return  <Breadcrumb style={{ margin: '16px 0' }}>
        {items.map((item, index) => (
            <Breadcrumb.Item key={index}>
                {item.route ? <Link to={item.route}>{item.name}</Link> : <>{context ?? item.name}</>}
            </Breadcrumb.Item>
        ))}
    </Breadcrumb>
}

export function BreadcrumbProvider(props: BreadcrumbContext) {
    return <BreadcrumbContext.Provider value={props.contextValue}>
        {props.children}
    </BreadcrumbContext.Provider>
}

export interface BreadcrumbContext {
    contextValue: string;
    children: React.ReactNode;
}

export interface CrudBreadcrumbProps {
    routingDefinitions: { [key: string]: RoutingDefinition };
}

export interface RoutingDefinition {
    route: string;
    breadcrumbEntries?: BreadcrumbEntry[];
}

export interface BreadcrumbEntry {
    name: string,
    route?: string
}
