# README #

Digimuth Components
React components and helpers tailored to be used together with CrudFramework

### Working components ###

 * AsyncButton - button that if promise is returned from onSubmit it will be in loading and disabled state until promise is resolved
 * AsyncButtonPopconfirm - same as above but in popconfirm
 * AsyncSwitch - same as above but as switch component
 * ColorPicker
 * CrudForm - easy to configure form specifically made to be compatible with CrudFramework backend and components
 * CrudFormModal
 * CrudFormSmall
 * CrudTable - crud framework compatible table
 * EmptyScreenInfo
 * EntityTransfer
 * FileUploader
 * SingleFileUploader
 * ProtectedRoute - route that when accessed by non authenticated user will trigger sign in flow (redirect to login screen or silent log in)
 * CrudBreadcrumb - breadcrumb component that utilizes Crud Framework route configuration and custom hook
 * CrudLayout - configurable layout with optional collapsible sider, header, and footer

### NOT working components (to be ported to Digimuth Components library) ###

 * AlertContainer
 * LanguageSelector - localizable components need to be moved here after figuring out a way to declare language dto
 * LocalizableFileUploader
 * LocalizableInput
 * LocalizableReadonly
 * LocalizableRichtextEditor
 * LocalizableVideo
 * RichtextEditor

### NOT migrated components from elearning ###

 * CypherCmsHeader

### Working helpers ###

 * arrayHelper
 * nameofHelper - provides syntax checking for strings that are supposed to represent property names (like nameof operator in C#)
 * orderableSort
 * string helpers - humanize method converts camel-case property names to user-friendly label names - it is used by default (can be replaced by custom names) in CrudForm and CrudTable - for example it will replace "firstName" with "First Name"

### NOT working helpers ###
 
 * mapper
 * localizableHelper
 
### Hooks ###

* useCurrentRoutingPath - returns currently matched CrudFramework compatible route configuration

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact