import { RcFile } from "antd/es/upload";
import {UploadFile, UploadProps} from "antd/lib/upload/interface";


export interface FileUploaderProps {
    onPreview?: (file: UploadFile<any>) => void;
    showPreviewIcon?: boolean;
    hideUploadButton?: boolean;
    uploadFilesCountMessage?: string;
    sizeValidationMessage?: string;
    buttonLabel?: string;
    defaultFileList?: Array<UploadFile<any>>;
    onChange?: (uploadedFileId: string[], allFiles: UploadFile<any>[]) => any;
    value?: number[];
    maxCount?: number;
    accept?: string;
    sizeLimitMb?: number,
    isFilePublic?: boolean,
    fileDownloadFunction: (id: number) => Promise<any>,
    fileUploadFunction: (fileOrigin: UploadFile<any>, httpRequestOptions: any, file: UploadFile) => Promise<any>,
    multipletFileUploadFunction?: (fileOrigin: UploadFile<any>[], httpRequestOptions: any) => Promise<any>,
    fileDeleteFunction?: (file: UploadFile) => Promise<any>,
    fileUploaderProps?: UploadProps,
    readonly?: boolean,
    openAsPreview?: boolean,
    enableDragAndDrop?: boolean,
    dragAndDropHint?: string,
    beforeUpload?: (file: RcFile) => void;
}
