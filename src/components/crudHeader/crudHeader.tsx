import {Header} from "antd/lib/layout/layout";
import styles from "./crudHeader.module.scss";
import {Avatar, Col, ConfigProvider, Divider, Drawer, Row} from "antd";
import {LoginOutlined, LogoutOutlined, MenuOutlined, UserOutlined} from "@ant-design/icons";
import React, {ReactElement, useState} from "react";
import {useNavigate} from "react-router-dom";
import {DrawerStyles} from "antd/es/drawer/DrawerPanel";
import {RoutingDefinition} from "../crudBreadcrumb/crudBreadcrumb";
import useCurrentRoutingPath from "../../hooks/useCurrentRoutingPath";
import AsyncButton from "../asyncButton/asyncButton";
import classNames from "classnames";

export interface MenuItem {
    displayName: string,
    route: string,
    icon: ReactElement;
}

interface CrudHeaderProps {
    routingPaths: { [key: string]: RoutingDefinition },
    userName?: string | undefined,
    logo: React.ReactElement,
    upperMenuItems?: MenuItem[],
    mainMenuItems?: MenuItem[],
    lowerMenuItems?: MenuItem[],
    headerChildren?: ReactElement,
    onLogout: () => Promise<any>,
    mobileMenuStyles?: DrawerStyles,
    mobileLogo: ReactElement,
    mainColor: string;
    logOutButtonClassName?: string;
}

export default function CrudHeader(props: CrudHeaderProps) {
    const [showMenu, setShowMenu] = useState<boolean>(false);

    const route = useCurrentRoutingPath(props.routingPaths)?.route || "";
    const navigate = useNavigate();

    const renderMenuItems = (items: MenuItem[] | undefined) => <div>
        {items?.map((item: MenuItem, index: number) =>
            <div
                key={index} className={styles.mobileMenuItem}
                onClick={() => {
                    navigate(item.route);
                    setShowMenu(false);
                }}
            >
                <div style={{
                    fontSize: "16px",
                    color: route === item.route ? props.mainColor : "#000000"
                }}>
                    {item.icon}
                </div>
                <div
                    style={{
                        marginLeft: "10px",
                        fontSize: "18px",
                        color: route === item.route ? props.mainColor : "#000000"
                    }}>
                    {item.displayName}
                </div>
            </div>)}
    </div>

    if (props.userName) {
        return <Header className={styles.exHeader}>
            <div style={{flex: 1}} id="header-title"/>
            <div onClick={() => navigate(props.routingPaths.mainPage.route)} className={styles.headerLogo}>
                {props.logo}
            </div>
            <div className={styles.headerChildren}>
                {props.headerChildren}
            </div>
            <div>
                <span className={styles.usernameText}>
                    {props.userName}
                </span>
                <ConfigProvider theme={{
                    token: {
                        colorBgSpotlight: props.mainColor,
                        paddingXS: 20
                    }
                }}>
                    <Avatar
                        className={styles.userAvatar}
                        size={40}
                        icon={<UserOutlined/>}
                    />
                </ConfigProvider>
            </div>
            <AsyncButton
                className={classNames(styles.logOutButton, props.logOutButtonClassName)}
                type="text"
                onClick={props.onLogout}
            >
                Wyloguj się <LogoutOutlined style={{fontSize: 24}}/>
            </AsyncButton>
            <MenuOutlined
                className={styles.burgerMenu}
                onClick={() => setShowMenu(true)}
            />
            <ConfigProvider theme={{token: {colorIcon: "white"}}}>
                <Drawer
                    open={showMenu}
                    width={"100%"}
                    styles={{
                        body: {
                            display: 'flex',
                            justifyContent: 'center',
                            ...props.mobileMenuStyles?.body
                        },
                        ...props.mobileMenuStyles
                    }}
                    onClose={() => setShowMenu(false)}
                    footer={null}
                    extra={<div className={styles.headerLogo} onClick={() => {
                        setShowMenu(false);
                        navigate(props.routingPaths.mainPage.route);
                    }}>
                        {props.mobileLogo}
                    </div>}
                >
                    <div className={styles.mobileItemsWrapper}>
                        <div className={`${styles.mobileItemsContainer}`}>
                            {renderMenuItems(props.upperMenuItems)}
                            <Divider style={{margin: "15px 0"}}/>
                            {renderMenuItems(props.mainMenuItems)}
                            <Divider style={{margin: "15px 0"}}/>
                            {renderMenuItems(props.lowerMenuItems)}
                        </div>
                    </div>
                    <div style={{position: "absolute", bottom: "50px"}}>
                        <div>{props.userName}</div>
                        <div onClick={props.onLogout}
                             style={{
                                 display: "flex",
                                 justifyContent: "center",
                                 alignItems: "center",
                                 marginTop: "30px"
                             }}
                        >
                            <LogoutOutlined style={{marginRight: "10px", fontSize: "18px"}}/>
                            <div>Wyloguj się</div>
                        </div>
                    </div>
                </Drawer>
            </ConfigProvider>
        </Header>;
    }

    return <Header className={styles.exHeader}>
        <Row>
            <div style={{flex: 1}}/>
            <Col>
                <AsyncButton type="text" onClick={props.onLogout} icon={<LoginOutlined/>}>
                    Login
                </AsyncButton>
            </Col>
        </Row>
    </Header>;
}