export interface EmptyScreenInfoProps {
    text: string;
    buttonText: string;
    onButtonClick: () => any
}
