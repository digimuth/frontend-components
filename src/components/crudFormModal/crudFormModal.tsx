import {Button, Form, Modal} from "antd";
import React, {ReactElement, useEffect} from "react";
import {CrudForm} from "../../index";
import {CrudFormProps} from "../crudForm/crudFormProps";

export default function CrudFormModal<TSubmit, TInitial>(props: CrudFormModalProps<TSubmit, TInitial>) {
    const [internalForm] = Form.useForm();
    const form = props.form ?? internalForm;

    const onModalCancelClicked = () => {
        props.onModalCancelClicked?.();
    }

    const onModalClosed = () => {
        form.resetFields();
    }

    useEffect(() => {
        if (props.isModalVisible) {
            form.resetFields();
        }
    }, [props.isModalVisible, form]);

    return <Modal
        {...props?.modalProps}
        destroyOnClose={props.destroyOnClose ?? false}
        forceRender={props.forceRender ?? false}
        open={props.isModalVisible}
        title={props.modalTitle}
        afterClose={onModalClosed}
        width={props?.width}
        footer={null}
        onCancel={onModalCancelClicked}
    >
        {props.preFormSection}
        <CrudForm
            {...props}
            onSubmit={props.onSubmit}
            form={props.form ?? form}
            saveButtonTitle={props.modalOkText || "Save"}
            extraActionsPosition={props.extraActionsPosition || "left"}
            extraActions={<Button style={{marginRight: "8px"}} onClick={onModalCancelClicked}>
                {props.modalCancelText || "Cancel"}
            </Button>}
            buttonsFloatPosition={props.buttonsFloatPosition || "right"}
        />
    </Modal>;
}


export interface CrudFormModalProps<TSubmit, TInitial> extends CrudFormProps<TSubmit, TInitial> {
    modalProps?: any,
    modalTitle: string,
    modalOkText?: string,
    forceRender?: boolean,
    width?: number | string,
    isModalVisible: boolean,
    destroyOnClose?: boolean,
    modalCancelText?: string,
    preFormSection?: ReactElement,
    onModalCancelClicked?: () => any,
    shouldClearModalOnClose?: boolean;
}
