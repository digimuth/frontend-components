import {Button, ButtonProps, Popover, Space, Spin, Table, TablePaginationConfig, TableProps} from "antd";
import {SpinIndicator} from "antd/lib/spin";
import {ColumnsType, ColumnType} from "antd/lib/table";
import {
    ExpandableConfig,
    FilterValue,
    SorterResult,
    TableCurrentDataSource,
    TableLocale
} from "antd/lib/table/interface";
import AsyncButton from "../asyncButton/asyncButton";
import React, {ReactNode, useState} from "react";
import {humanize} from "../../helpers/stringHelpers";
import AsyncButtonPopconfirm from "../asyncButtonPopconfirm/asyncButtonPopconfirm";
import {SortOrder} from "antd/es/table/interface";
import { InfoCircleOutlined } from "@ant-design/icons";

export default function CrudTable<T extends { id?: IdType }, IdType extends number | string>(props: CrudTableProps<T, IdType>) {
    const params = new URLSearchParams(window.location.search);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const {pageNumber, pageSize} = Object.fromEntries(params.entries());
    const [innerPageNumber, setInnerPageNumber] = useState<number>(pageNumber !== undefined ? +pageNumber : 1);
    const [innerPageSize, setInnerPageSize] = useState<number>(pageSize !== undefined ? +pageSize : 10);

    const actionsColumn: ColumnType<T> = {
        key: "actions",
        title: "",
        fixed: props.actionsFixed === undefined ? "right" : props.actionsFixed,
        render: (value: any, record: T) => {
            const isEditButtonDisabled = props.editButtonDisableCondition && props.editButtonDisableCondition(record);
            const isDeleteButtonDisabled = props.deleteButtonDisableCondition && props.deleteButtonDisableCondition(record);
            return <>
                <Space>
                    {props.onEditClicked ? <AsyncButton
                        disabled={isEditButtonDisabled}
                        onClick={() => props.onEditClicked!(record.id!)}
                    >{props.editButtonTitle || "Edit"}</AsyncButton> : null}
                    {props.onDeleteClicked ? <AsyncButtonPopconfirm
                        danger
                        title={props.deleteConfirmMessage || "Are you sure to delete this item?"}
                        onConfirm={async () => {
                            setIsLoading(true);
                            try {
                                await props.onDeleteClicked!(record.id!);
                            } finally {
                                setIsLoading(false);
                            }
                        }}
                        onCancel={() => {
                        }}
                        okText={"Ok"}
                        cancelText={props.cancelButtonTitle || "Cancel"}
                        disabled={isDeleteButtonDisabled}
                    >
                        {props.deleteButtonTitle || "Delete"}
                    </AsyncButtonPopconfirm> : null}

                    {props.customRowActions?.(record)}
                </Space>
            </>

        }
    };

    const queryStringToDictionary = (): Record<string, string[]> => {
        const result: Record<string, string[]> = {};

        params.forEach((value, key) => {
            if (result[key]) {
                result[key].push(value);
            } else {
                result[key] = [value];
            }
        });

        return result;
    }

    props.columns.forEach(c => {
        const column = c as (ColumnType<T> & { description?: string });
        if (!column.dataIndex) {
            column.dataIndex = column.key?.toString();
        }

        if (!column.title) {
            column.title = humanize(column.key as string);
        }

        if (column.description) {
            column.title = <>{column.title} <Popover placement="bottom" content={column.description}><InfoCircleOutlined /></Popover></>
        }
    });

    Object.entries(queryStringToDictionary())
        .map(([key, value]) => ({key, value}))
        .map(x => {
            const column = props.columns.find((c: any) => c.key === x.key) as ColumnType<T>;
            if (column) {
                if (x.value.includes("ascend") || x.value.includes("descend")) {
                    column.defaultSortOrder = x.value[0] as SortOrder;
                } else {
                    column.defaultFilteredValue = x.value;
                }
            }
        });

    const columns = [...props.columns, actionsColumn];

    const createQueryString = (name: string, value?: string, append?: boolean) => {
        if (!value) {
            params.delete(name);
            window.history.replaceState({}, document.title, `${window.location.pathname}?${params.toString()}`);
            return;
        }
        if (append) {
            params.append(name, value);
        } else {
            params.set(name, value);
        }
        window.history.replaceState({}, document.title, `${window.location.pathname}?${params.toString()}`);
    };

    const onChange = (pagination: TablePaginationConfig,
                      filters: Record<string, FilterValue | null>,
                      sorter: any,
                      extra: TableCurrentDataSource<T>) => {

        createQueryString("pageNumber", pagination.current!.toString());
        createQueryString("pageSize", pagination.pageSize!.toString());

        setInnerPageNumber(pagination.current!);
        setInnerPageSize(pagination.pageSize!);

        const columnsWithFilter = props.columns.filter(x => x.filters);
        columnsWithFilter.map(x => createQueryString(x.key!.toString()))
        Object.entries(filters)
            .map(([key, filter]) => ({key, filter}))
            .map(x => x.filter
                ?.map(f => createQueryString(x.key, f.toString(), true))
            )

        const columnsWithSorter = props.columns.filter(x => x.sorter);
        columnsWithSorter.map(column => {
            if (sorter.columnKey === column.key!.toString()) {
                if (sorter.order) {
                    createQueryString(column.key!.toString(), sorter.order.toString());
                    columnsWithSorter
                        .filter(x => x.key !== column.key)
                        .map(x => createQueryString(x.key!.toString()))
                } else {
                    createQueryString(column.key!.toString());
                }
            }
        });
    }

    return <Spin indicator={props.loadingIndicator} spinning={!props.data || isLoading || props.isLoading || false}
                 delay={props.loadingIndicatorDelay || 400}>
        <Table expandable={props.expandable}
               pagination={props.pagination ? props.pagination : props.useFilterAndSortQueryParameters ? {
                   showSizeChanger: true,
                   current: innerPageNumber,
                   defaultPageSize: innerPageSize
               } : undefined}
               locale={props.locale} onChange={props.useFilterAndSortQueryParameters ? onChange : undefined}
               columns={columns} dataSource={props.data!} rowKey="id" scroll={{x: true}}
               onRow={(record: T) => {
                   return {
                       onClick: () => props.onRowClick?.(record)
                   }
               }}
               {...props.tableProps}>
        </Table>
        {(props.onAddClicked || props.customActions) && <>
            <br/>
            <Space>
                {props.onAddClicked ? <Button
                    type="primary"
                    onClick={props.onAddClicked}
                    {...props.submitButtonProps}
                >
                    {props.addButtonTitle || "+ Add"}
                </Button> : null}
                {props.customActions?.()}
            </Space>
        </>}
    </Spin>;
}

interface CrudTableProps<T, IdType extends string | number = number> {
    loadingIndicatorDelay?: number,
    isLoading?: boolean,
    loadingIndicator?: SpinIndicator,
    data: T[] | null,
    onAddClicked?: React.MouseEventHandler<HTMLElement>,
    onEditClicked?: (id: IdType) => any,
    onRowClick?: (record: T) => void,
    onDeleteClicked?: (id: IdType) => any,
    columns: ColumnsType<T> |  (ColumnType<T> & { description?: string })[],
    onChange?: (pagination: TablePaginationConfig,
                filters: Record<string, FilterValue | null>,
                sorter: SorterResult<T> | SorterResult<T>[],
                extra: TableCurrentDataSource<T>) => void,
    customRowActions?: (record: T) => ReactNode,
    customActions?: () => ReactNode,
    actionsFixed?: 'left' | 'right' | boolean,
    addButtonTitle?: string,
    editButtonTitle?: string,
    deleteButtonTitle?: string,
    deleteConfirmMessage?: string,
    cancelButtonTitle?: string,
    locale?: TableLocale,
    submitButtonProps?: ButtonProps & React.RefAttributes<HTMLElement>,
    pagination?: TablePaginationConfig | false,
    expandable?: ExpandableConfig<T>,
    editButtonDisableCondition?: (record: T) => boolean,
    deleteButtonDisableCondition?: (record: T) => boolean,
    useFilterAndSortQueryParameters?: boolean;
    tableProps?: TableProps<T>
}

export interface CrudTableWrapperProps<T, IdType extends number | string = number> {
    data: T[] | null,
    onAddClicked?: () => any,
    onEditClicked?: (id: IdType) => any,
    onDeleteClicked?: (id: IdType) => any,
    isLoading?: boolean;
}
