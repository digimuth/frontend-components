import { FormInstance, Rule } from "antd/lib/form";
import { ReactElement, ReactNode } from "react";

export interface CrudFormProps<TSubmit, TInitial> {
    form?: FormInstance<any>,
    extraActions?: ReactNode,
    extraActionsPosition?: "right" | "left",
    buttonsFloatPosition?: "right" | "left",
    saveButtonTitle?: string,
    formItems: CrudFormItemProps[],
    isSubmitDisabled?: boolean,
    onSubmit: (value: TSubmit) => Promise<any>,
    onFinishFailed?: () => void,
    formProps?: any,
    tailProps?: any,
    submitButtonProps?: any,
    initialValues?: TInitial,
    underInputActions?: ReactNode,
    setIsLoadingExternal?: (isLoading: boolean) => void,
    headFormItems?: any,
    submitInitialDisable?: boolean;
}

export interface CrudFormItemsWrapperProps {
    formItems: CrudFormItemProps[]
}

export interface CrudFormWrapperProps<TSubmit> {
    onSubmit: (value: TSubmit) => Promise<any>
}

export interface CrudFormItemProps {
    className?: string;
    validateTrigger?: string | false | string[];
    label?: string,
    hideLabel?: boolean,
    name: string | string[],
    rules?: Rule[],
    children?: ReactElement,
    dependencies?: any,
    hasFeedback?: boolean,
    style?: any,
    trigger?: string,
    valuePropName?: string
}
