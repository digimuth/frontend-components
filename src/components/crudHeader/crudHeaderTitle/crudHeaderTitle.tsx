import Title from "antd/es/typography/Title";
import { ReactNode } from "react";
import styles from "./crudHeaderTitle.module.scss";
import {createPortal} from "react-dom";

export default function CrudHeaderTitle(props: { children: ReactNode }) {
    const titleContainer = document.getElementById("header-title");

    if (!titleContainer) {
        return null;
    }

    return createPortal(<Title level={3} className={styles.title}>{props.children}</Title>, titleContainer);
}