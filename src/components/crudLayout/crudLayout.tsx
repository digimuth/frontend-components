import {Layout, SiderProps} from "antd";
import ConfigProvider, { ConfigProviderProps } from "antd/es/config-provider";
import React from "react";
import {useState} from "react";

const {Content, Footer, Sider} = Layout;

export default function CrudLayout(props: CrudLayoutProps) {
    return <ConfigProvider {...props.configProviderProps}>
        <CrudLayoutInternal {...props} />
    </ConfigProvider>
}

function CrudLayoutInternal(props: CrudLayoutProps) {
    const [isCollapsed, setIsCollapsed] = useState<boolean>(false);

    if (props.siderFullHeight) {
        return <Layout style={{minHeight: '100dvh'}}>
            {!props.showSider ? null :
                <Sider breakpoint="sm" collapsible collapsed={isCollapsed}
                       onCollapse={() => setIsCollapsed(!isCollapsed)}>
                    {props.siderMenu}
                </Sider>
            }
            <Layout className="site-layout">
                <>
                    {props.header}
                </>
                <Content>
                    <>
                        {props.breadcrumb}
                    </>
                    <>
                        {props.children}
                    </>
                </Content>
                {props.footerContent && <Footer style={{textAlign: 'center'}}>
                    {props.footerContent}
                </Footer>}
            </Layout>
        </Layout>
    }

    return <Layout style={{minHeight: '100dvh'}}>
        <>
            {props.header}
        </>
        <Layout className="site-layout">

            {!props.showSider ? null :
                <Sider breakpoint="sm" collapsible collapsed={isCollapsed}
                       onCollapse={() => setIsCollapsed(!isCollapsed)} {...props.siderProps}>
                    {props.siderMenu}
                </Sider>
            }
            <Content>
                <>
                    {props.breadcrumb}
                </>
                <>
                    {props.children}
                </>
            </Content>
        </Layout>
        {props.footerContent && <Footer style={{textAlign: 'center'}}>
            {props.footerContent}
        </Footer>}
    </Layout>;
}

export interface CrudLayoutProps {
    children: JSX.Element;
    breadcrumb: JSX.Element;
    siderMenu: JSX.Element;
    header: JSX.Element;
    footerContent?: JSX.Element;
    showSider: boolean;
    siderFullHeight?: boolean;
    configProviderProps?: ConfigProviderProps;
    siderProps?: SiderProps
}
