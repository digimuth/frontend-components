import {UploadFile, UploadProps} from "antd/lib/upload/interface";
import React, {forwardRef, useImperativeHandle, useRef} from "react";
import FileUploader from "./fileUploader";
import { RcFile } from "antd/es/upload";

type FileUploaderHandle = React.ElementRef<typeof FileUploader>;

function getSingleElementArrayOrEmpty<T>(element?: T): T[] {
    return element ? [element] : [];
}

const SingleFileUploader = forwardRef((props: SingleFileUploaderProps, ref: React.ForwardedRef<any>) => {

    const fileListRef = useRef(getSingleElementArrayOrEmpty(props.defaultFile));
    const fileUploaderRef = useRef<FileUploaderHandle>();

    useImperativeHandle(ref, () => ({
        async onFormFinish() {
            return await fileUploaderRef.current.onFormFinish();
        }
    }));

    const onChange = (uploadedFileIds: string[], file: UploadFile<any>[]) => {
        props.onChange?.(uploadedFileIds[0], file[0]);
    };

    const defaultFileList = props.defaultFile !== fileListRef.current[0]
        ? (fileListRef.current = getSingleElementArrayOrEmpty(props.defaultFile))
        : fileListRef.current;
    const value = props.value ? [props.value] : undefined;


    return <FileUploader
        ref={fileUploaderRef}
        defaultFileList={defaultFileList}
        onChange={onChange}
        value={value}
        accept={props.accept}
        sizeLimitMb={props.sizeLimitMb}
        beforeUpload={props.beforeUpload}
        maxCount={1}
        isFilePublic={props.isFilePublic}
        sizeValidationMessage={props.sizeValidationMessage}
        fileDownloadFunction={props.fileDownloadFunction}
        fileUploadFunction={props.fileUploadFunction}
        fileDeleteFunction={props.fileDeleteFunction}
        buttonLabel={props.buttonLabel}
        uploadFilesCountMessage={props.uploadFilesCountMessage}
        fileUploaderProps={props.fileUploaderProps}
        hideUploadButton={props.hideUploadButton}
        readonly={props.readonly}
        openAsPreview={props.openAsPreview}
        onPreview={props.onPreview}
        showPreviewIcon={props.showPreviewIcon}
        dragAndDropHint={props.dragAndDropHint}
        enableDragAndDrop={props.enableDragAndDrop}
    />
});

export interface SingleFileUploaderProps {
    showPreviewIcon?: boolean,
    onPreview?: (file: UploadFile<any>) => void,
    hideUploadButton?: boolean,
    uploadFilesCountMessage?: string,
    buttonLabel?: string,
    sizeValidationMessage?: string,
    fileUploadFunction: (fileOrigin: UploadFile<any>, httpRequestOptions: any, file: UploadFile) => Promise<any>,
    fileDeleteFunction?: (file: UploadFile<any>) => Promise<any>,
    fileDownloadFunction: (id: number) => Promise<any>,
    defaultFile?: UploadFile<any>,
    onChange?: (uploadedFileId?: string, file?: UploadFile<any>) => any,
    value?: number,
    accept?: string,
    sizeLimitMb?: number,
    useOrganisationFileApi?: boolean,
    isFilePublic?: boolean,
    fileUploaderProps?: UploadProps,
    readonly?: boolean,
    openAsPreview?: boolean,
    beforeUpload?: (file: RcFile) => void;
    enableDragAndDrop?: boolean,
    dragAndDropHint?: string,
}

export default SingleFileUploader;
